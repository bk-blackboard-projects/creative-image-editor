$(function() {
	// GET THE WIDTH AND HEIGHT VALUES FROM THE QUERY STRING
	var imgWidth = getQueryValuePos("w");
	var imgHeight = getQueryValuePos("h");
	
	// IF THE WIDTH AND HEIGHT VALUES ARE SET
	if(imgWidth != null && imgHeight != null) {
		// BUILD THE INTERFACE
		cropItEditor(imgWidth, imgHeight);

		// SET WIDTH AND HEIGHT IN INPUTS
		$("#cropit-width").val(imgWidth);
		$("#cropit-height").val(imgHeight);
		
		// UPDATE COOKIES
		createCookie("creativeEditorW", imgWidth, 365);
		createCookie("creativeEditorH", imgHeight, 365);
	}

	// WIDTH AND HEIGHT VALUES ARE NOT SET TO USE DEFAULT
	else {
		var storedWidth = 0;
		var storedHeight = 0;
		
		// DIMENSIONS HAVE BEEN STORED
		if(readCookie("creativeEditorW") && readCookie("creativeEditorH")) {
			storedWidth = readCookie("creativeEditorW");
			storedHeight = readCookie("creativeEditorH");
		}
		
		// NO DIMENSIONS HAVE BEEN STORED SO USE DEFAULTS
		else {
			storedWidth = 700;
			storedHeight = 400;
			createCookie("creativeEditorW", 700, 365);
			createCookie("creativeEditorH", 400, 365);
		}
	
		// RELOAD THE PAGE WITH AN UPDATED QUERY STRING
		document.location.search = "?w=" + storedWidth + "&h=" + storedHeight;

		// SET WIDTH AND HEIGHT INPUTS
		$("#cropit-width").val(storedWidth);
		$("#cropit-height").val(storedHeight);
	}
	
	// CHECK TO SEE IF THE CURRENT DIMENSIONS ARE WIDER THAN THE USERS SCREEN
	if(($(".cropit-preview").width() + 42) > $(window).width()) {
		$(".section.main").addClass("image-overflow");
	} else {
		$(".section.main").removeClass("image-overflow");
	}

	// CLOSE LIGHTBOX
	$("#light-box-outer, #light-box-close").click(function(e) {
		e.preventDefault();
		$("#light-box-outer").fadeOut();
		$("#cropit-upload").focus();
	});
	$("#light-box").click(function(e) {
		e.stopImmediatePropagation();
	});
});

function cropItEditor(imgWidth, imgHeight) {
	// SET CROPIT EDITOR
	$(".image-editor").cropit({
		width: imgWidth,
		height: imgHeight,
		imageBackground: false,
		onImageError: function(error) {
			// SHOW ERROR MESSAGE THAT WAS PROVIDED - MOST LIKELY THAT IMAGE IS TOO SMALL
			lightBox("<h2>Uh oh!</h2><p class='light-box-message'>" + error.message + "</p>");
			
			// MAKE SURE THE DOWNLOAD BUTTON IS HIDDEN
			$("#cropit-export").addClass("hidden");
		},
		onFileChange: function() {
			// UPDATE THE DOWNLOAD BUTTON ATTRIBUTES FOR THE IMAGE DOWNLOAD
			getImageData();
			
			// SHOW THE DOWNLOAD BUTTON
			$("#cropit-export").removeClass("hidden");
		},
		onOffsetChange: function() {
			// UPDATE THE DOWNLOAD BUTTON ATTRIBUTES FOR THE IMAGE DOWNLOAD
			getImageData();
		}
	});
	
	// ROTATE CLOCKWISE BUTTON
	$(".cropit-rotate-control.rotate-cw").click(function() {
		$(".image-editor").cropit("rotateCW");
	});
	
	// ROTATE COUNTERCLOCKWISE BUTTON
	$(".cropit-rotate-control.rotate-ccw").click(function() {
		$(".image-editor").cropit("rotateCCW");
	});
	
	// FLIP IMAGE HORIZONTALLY
	// THE PLUGIN DOES NOT COME WITH THIS OPTION - IT WAS CUSTOM BUILT IN jquery.cropit.js
	$(".cropit-rotate-control.flip-horiz").click(function() {
		if($(".cropit-preview-image[src]").length) {
			$(".image-editor").cropit("flipH");
		}
	});
	
	// DIMENSION INPUT FOCUS
	$(".cropit-dimension").focus(function() {
		$("+ .cropit-tooltip", this).attr("aria-hidden", "false");
	});
	
	// DIMENSION INPUT BLUR
	$(".cropit-dimension").blur(function() {
		$("+ .cropit-tooltip", this).attr("aria-hidden", "true");
	});
	
	// UPLOAD IMAGE BUTTON
	$("#cropit-upload").click(function() {
		$(".cropit-image-input").click();
	});
	
	// SUBMIT NEW WIDTH AND HEIGHT DIMENSIONS
	$("#cropit-dimensions").submit(function(e) {
		e.preventDefault();
		
		// RELOAD THE PAGE WITH AN UPDATED QUERY STRING
		document.location.search = "?w=" + $("#cropit-width").val() + "&h=" + $("#cropit-height").val();
	});
}

function getImageData() {
	// MAKE SURE THERE IS AN IMAGE IN THE EDITOR
	if($(".cropit-preview-image-container img").length) {
        // WAIT FOR IMAGE TO LOAD IF IT HASNT LOADED YET
        if(!$(".cropit-preview-image-container img").hasClass("loaded")) {
            $(".cropit-preview-image-container img").load(function() {
                setDownloadAttrs();

                // ADD LOADED CLASS TO IMAGE
                $(this).addClass("loaded");
            });
        }
        
        // IMAGE IS ALREADY LOADED
        else {
            setDownloadAttrs();
        }
	}
}

function setDownloadAttrs() {
    // GET THE IMAGE PATH FROM THE FILE INPUT
    var imagePath = $(".cropit-image-input").val().split("\\");

    // GET THE IMAGE NAME
    var imageName = imagePath[imagePath.length - 1].split(".");

    // ADD "-cropped" TO THE END OF THE IMAGE NAME AND REATTACH THE EXTENSION
    var newImageName = imageName[0] + "-cropped." + imageName[1].toLowerCase();
    var contentType = "image/" + imageName[1].toLowerCase();

    // GET THE IMAGE DATA URI FROM THE cropit PLUGIN AND THEN TURN IT INTO A BLOB
    // USING A BLOB IS BEST BECAUSE CHROME FAILS IF THE DATA URI IS TOO LONG
    var imageData = dataURItoBlob($(".image-editor").cropit("export", {
        type: (imageName[1].toLowerCase() == "jpg" ? "image/jpeg" : "image/" + imageName[1].toLowerCase()),
        quality: .8
    }), contentType);

    // UPDATE THE DOWNLOAD BUTTON ATTRIBUTES
    $("#cropit-export").attr("href", window.URL.createObjectURL(imageData)).attr("download", newImageName);
}

// CREDIT TO https://gist.github.com/fupslot/5015897
function dataURItoBlob(dataURI, contentType, callback) {
    // convert base64 to raw binary data held in a string
    // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
    var byteString = atob(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

    // write the bytes of the string to an ArrayBuffer
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    // write the ArrayBuffer to a blob, and you're done
    var bb = new Blob([ab], {type: contentType});
    return bb;
}

function getQueryValuePos(key) {
	// LOOPS THROUGH THE ARRAY OF QUERY STRING ITEMS AND SEARCHES FOR THE KEY THAT WAS PASSED
	var querys = getQueries();
	for(var k in querys) {
		if(querys[k][0] == key) {
			return querys[k][1];
		}
	}
	
	// RETURN NULL IF KEY WAS NOT FOUND
	return null;
}

function getQueries() {
	// BUILD AN ARRAY OF QUERY STRING ITEMS
	var query = location.search.substring(1);
	var queries = [];
	var vars = query.split("&");
	$.each(vars, function(i){
		queries[i] = [];
		queries[i] = vars[i].split("=");
	});

	return queries;
}

function lightBox(htmlMessage) {
	$("#light-box-outer #light-box-content").html(htmlMessage);
	$("#light-box-outer").fadeIn();
	$("#light-box-close").focus();
}

function createCookie(name, value, days) {
	var expires;
		
	if (days) {
		var date = new Date();
		date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
		expires = "; expires=" + date.toGMTString();
	} else {
		expires = "";
	}
	document.cookie = escape(name) + "=" + escape(value) + expires + "; path=/";
}
	
function readCookie(name) {
	var nameEQ = escape(name) + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) === ' ') c = c.substring(1, c.length);
		if (c.indexOf(nameEQ) === 0) return unescape(c.substring(nameEQ.length, c.length));
	}
	return null;
}
	
function eraseCookie(name) {
	createCookie(name, "", -1);
}